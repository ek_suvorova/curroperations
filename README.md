##REST-сервис с двумя методами action и stat:
  * на запрос action сервис принимает код валюты (например, 840), с которым он делает исходящий запрос ко внешнему сервису (метод GetCursOnDateXML) и в зависимости от результата увеличивает один или несколько счётчиков (см. ниже).
  * На запрос stat сервис возвращает значения всех счётчиков. 

Приложение ведет в БД три счётчика внешних запросов:
  1.Общий счётчик запросов
  2. Общий счётчик успешных запросов
  3. Счётчик успешных запросов, относящихся к конкретному коду валюты

##WSDL внешнего сервиса:
[http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?wsdl](http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?wsdl)

##Генерация классов для взаимодействия с soap-сервисом:
mvn jaxb2:generate

##Запуск приложения:
mvn spring-boot:run

##H2 console:
spring.datasource.url=jdbc:h2:mem:actionstatbd
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=admin
spring.datasource.password=admin
[http://localhost:8080/h2-console/](http://localhost:8080/h2-console/)