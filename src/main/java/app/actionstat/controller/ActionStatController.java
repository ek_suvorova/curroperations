package app.actionstat.controller;

import app.actionstat.model.ActionRequest;
import app.actionstat.model.StatResponse;
import app.cbr.service.CBRService;
import app.data.service.DataManagerService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cbr.schemas.GetCursOnDateXMLResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.cbr.schemas.ValuteData;

import java.util.List;


@RestController
public class ActionStatController {
    final private DataManagerService dataManagerService;
    final private CBRService cbrService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ActionStatController.class);

    @Autowired
    public ActionStatController(DataManagerService dataManagerService, CBRService cbrService) {
        this.dataManagerService = dataManagerService;
        this.cbrService = cbrService;
    }

    @PostMapping(value = "/action", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity action(@RequestBody ActionRequest request) {
        ResponseEntity responseEntity;
        try {
            String currCode = request.getCurrCode();
            GetCursOnDateXMLResponse response = cbrService.getCursOnDateXML();

            List valuteData = response.getGetCursOnDateXMLResult().getContent();
            boolean containedCurrCode = false;
            if ((!valuteData.isEmpty())) {
                for (ValuteData.ValuteCursOnDate valuteCurs : ((ValuteData) valuteData.get(0)).getValuteCursOnDate()) {
                    if (valuteCurs.getVcode().equals(currCode)) {
                        containedCurrCode = true;
                        break;
                    }
                }
            }
            dataManagerService.updateStat(containedCurrCode, currCode);
            responseEntity = new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            dataManagerService.updateStat(false, null);
            responseEntity = new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping(value = "/stat")
    public ResponseEntity<Object> stat() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        StatResponse response = dataManagerService.getAllStat();
        return new ResponseEntity(response, httpHeaders, HttpStatus.OK);
    }
}
