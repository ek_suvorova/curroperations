package app.actionstat.model;

public class ActionRequest {
    private String currCode;

    public String getCurrCode() {
        return currCode;
    }

    public void setCurrCode(String currCode) {
        this.currCode = currCode;
    }
}
