package app.actionstat.model;

import java.io.Serializable;
import java.util.Map;

public class StatResponse implements Serializable {
    private Long allCounter;
    private Long successCounter;
    private Map<String, Long> successPerCurrencyCounter;

    public Long getAllCounter() {
        return allCounter;
    }

    public void setAllCounter(Long allCounter) {
        this.allCounter = allCounter;
    }

    public Long getSuccessCounter() {
        return successCounter;
    }

    public void setSuccessCounter(Long successCounter) {
        this.successCounter = successCounter;
    }

    public Map<String, Long> getSuccessPerCurrencyCounter() {
        return successPerCurrencyCounter;
    }

    public void setSuccessPerCurrencyCounter(Map<String, Long> successPerCurrencyCounter) {
        this.successPerCurrencyCounter = successPerCurrencyCounter;
    }
}
