package app.cbr;

import ru.cbr.schemas.GetCursOnDateXML;
import ru.cbr.schemas.GetCursOnDateXMLResponse;
import ru.cbr.schemas.GetLatestDateTime;
import ru.cbr.schemas.GetLatestDateTimeResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.Date;

public class CBRClient extends WebServiceGatewaySupport {

    private static final String CBR_URL = "http://web.cbr.ru/";
    private static final Logger LOGGER = LoggerFactory.getLogger(CBRClient.class);

    private GetLatestDateTimeResponse getLatestDateTime() {
        GetLatestDateTime request = new GetLatestDateTime();
        LOGGER.info("Requesting getLatestDateTime at:" + new Date().toString());
        return (GetLatestDateTimeResponse) getWebServiceTemplate()
                .marshalSendAndReceive(request, new SoapActionCallback(CBR_URL + "GetLatestDateTime"));
    }

    public GetCursOnDateXMLResponse getCursOnDateXML() {
        GetLatestDateTimeResponse getLatestDateTimeResponse = getLatestDateTime();
        GetCursOnDateXML request = new GetCursOnDateXML();
        request.setOnDate(getLatestDateTimeResponse.getGetLatestDateTimeResult());
        LOGGER.info("Requesting getCursOnDateXML for " + getLatestDateTimeResponse.getGetLatestDateTimeResult().toString());
        return (GetCursOnDateXMLResponse) getWebServiceTemplate()
                .marshalSendAndReceive(request, new SoapActionCallback(CBR_URL + "GetCursOnDateXML"));
    }

}