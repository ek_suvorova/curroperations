package app.cbr.service;

import ru.cbr.schemas.GetCursOnDateXMLResponse;

/**
 * Взаимодействие с клиентом soap-сервиса
 */
public interface CBRService {
    GetCursOnDateXMLResponse getCursOnDateXML();
}
