package app.cbr.service;

import app.cbr.CBRClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.cbr.schemas.GetCursOnDateXMLResponse;

@Component
public class CBRServiceImpl implements CBRService {
    final private CBRClient cbrClient;

    @Autowired
    public CBRServiceImpl(CBRClient cbrClient) {
        this.cbrClient = cbrClient;
    }

    @Override
    public GetCursOnDateXMLResponse getCursOnDateXML() {
        return cbrClient.getCursOnDateXML();
    }
}
