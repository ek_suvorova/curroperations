package app.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OperationsCounter {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private Long allOperations;
    private Long successOperstions;

    public void setAllOperations(Long allOperations) {
        this.allOperations = allOperations;
    }

    public void setSuccessOperations(Long successOperstions) {
        this.successOperstions = successOperstions;
    }

    public Long getAllOperations() {
        return allOperations;
    }

    public Long getSuccessOperstions() {
        return successOperstions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
