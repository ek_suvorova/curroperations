package app.data.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class OperationsPerCurrencyCounter {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String currencyCode;
    private Long successCounter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setSuccessCounter(Long successCounter) {
        this.successCounter = successCounter;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Long getSuccessCounter() {
        return successCounter;
    }
}
