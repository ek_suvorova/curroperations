package app.data.repository;

import app.data.entity.OperationsCounter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationsCounterRepository extends CrudRepository<OperationsCounter, Long> {

}
