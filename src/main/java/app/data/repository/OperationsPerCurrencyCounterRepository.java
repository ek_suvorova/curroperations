package app.data.repository;

import app.data.entity.OperationsPerCurrencyCounter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationsPerCurrencyCounterRepository extends CrudRepository<OperationsPerCurrencyCounter, Long> {
}
