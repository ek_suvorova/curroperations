package app.data.service;

import app.actionstat.model.StatResponse;

public interface DataManagerService {
    StatResponse getAllStat();
    void updateStat(boolean success, String currencyCode);
}
