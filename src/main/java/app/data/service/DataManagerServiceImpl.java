package app.data.service;

import app.data.entity.OperationsCounter;
import app.data.entity.OperationsPerCurrencyCounter;
import app.actionstat.model.StatResponse;
import app.data.repository.OperationsCounterRepository;
import app.data.repository.OperationsPerCurrencyCounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DataManagerServiceImpl implements DataManagerService {
    private final OperationsCounterRepository operationsCounterRepository;
    private final OperationsPerCurrencyCounterRepository operationsPerCurrencyCounterRepository;

    @Autowired
    public DataManagerServiceImpl(OperationsCounterRepository operationsCounterRepository,
                                  OperationsPerCurrencyCounterRepository operationsPerCurrencyCounterRepository) {
        this.operationsCounterRepository = operationsCounterRepository;
        this.operationsPerCurrencyCounterRepository = operationsPerCurrencyCounterRepository;
    }

    @Override
    public StatResponse getAllStat() {
        StatResponse response = new StatResponse();

        OperationsCounter operationsCounter = getOperationsCounter();
        response.setAllCounter(operationsCounter.getAllOperations());
        response.setSuccessCounter(operationsCounter.getSuccessOperstions());

        List<OperationsPerCurrencyCounter> currencyCounterList = getOperationsPerCurrencyCounter();
        Map<String, Long> currencyMap = currencyCounterList.stream().collect(Collectors.toMap(OperationsPerCurrencyCounter::getCurrencyCode,
                OperationsPerCurrencyCounter::getSuccessCounter));
        response.setSuccessPerCurrencyCounter(currencyMap);

        return response;
    }

    //Can be synchronized by other ways if app will be run on multiple JVM instances
    @Override
    public synchronized void updateStat(boolean success, String currencyCode) {
        OperationsCounter operationsCounter = getOperationsCounter();
        Long allOperations = operationsCounter.getAllOperations();
        operationsCounter.setAllOperations(allOperations == null ? 1L : ++allOperations);
        if (success) {
            Long successOperations = operationsCounter.getSuccessOperstions();
            operationsCounter.setSuccessOperations(successOperations == null? 1L : ++successOperations);

            List<OperationsPerCurrencyCounter> currencyCounterList = getOperationsPerCurrencyCounter();
            OperationsPerCurrencyCounter operationsPerCurrencyCounter = currencyCounterList.stream()
                    .filter(counter -> currencyCode.equals(counter.getCurrencyCode())).findFirst().orElse(null);
            if (operationsPerCurrencyCounter == null) {
                operationsPerCurrencyCounter = new OperationsPerCurrencyCounter();
                operationsPerCurrencyCounter.setCurrencyCode(currencyCode);
                operationsPerCurrencyCounter.setSuccessCounter(1L);
            } else {
                long counter = operationsPerCurrencyCounter.getSuccessCounter();
                operationsPerCurrencyCounter.setSuccessCounter(++counter);
            }
            operationsPerCurrencyCounterRepository.save(operationsPerCurrencyCounter);
        }
        operationsCounterRepository.save(operationsCounter);
    }

    private OperationsCounter getOperationsCounter() {
        OperationsCounter operationsCounter = new OperationsCounter();
        Object counters = operationsCounterRepository.findAll();
        if ((counters != null) && (((List<OperationsCounter>) counters).size() != 0)) {
            List<OperationsCounter> operationsCounterList = (List<OperationsCounter>) counters;
            operationsCounter = operationsCounterList.get(0);
        }
        return operationsCounter;
    }

    private List<OperationsPerCurrencyCounter> getOperationsPerCurrencyCounter() {
        List<OperationsPerCurrencyCounter> currencyCounterList = new ArrayList<>();
        Object currencyCounters = operationsPerCurrencyCounterRepository.findAll();
        if ((currencyCounters != null) && (((List<OperationsPerCurrencyCounter>) currencyCounters).size() != 0)) {
            List<OperationsPerCurrencyCounter> operationsPerCurrencyCounterList = (List<OperationsPerCurrencyCounter>) currencyCounters;
            for (OperationsPerCurrencyCounter operationsPerCurrencyCounter : operationsPerCurrencyCounterList) {
                currencyCounterList.add(operationsPerCurrencyCounter);
            }
        }
        return currencyCounterList;
    }
}
