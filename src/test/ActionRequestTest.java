import app.actionstat.model.ActionRequest;
import app.actionstat.model.StatResponse;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;

public class ActionRequestTest extends AbstractBaseTest {
    private static final int THREADS_NUMBER = 10;

    private static final Logger LOGGER = LoggerFactory.getLogger(ActionRequestTest.class);

    @Test
    public void sendManyActionRequests() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(THREADS_NUMBER);
        ActionRequest actionRequest = new ActionRequest();
        actionRequest.setCurrCode(RUR_CODE);
        String actionRequestJson = mapToJson(actionRequest);
        for (int i = 0; i < THREADS_NUMBER; i++) {
            executor.execute(() ->
            {
                try {
                    mvc.perform(MockMvcRequestBuilders.post(ACTION_SERVICE_URL)
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .content(actionRequestJson)
                            .accept(MediaType.APPLICATION_JSON_VALUE))
                            .andReturn();
                } catch (Exception e) {
                    LOGGER.error(e.getMessage());
                }
            });
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        LOGGER.info("Sent ActionRequests number: " + THREADS_NUMBER);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(STAT_SERVICE_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);
        String content = mvcResult.getResponse().getContentAsString();
        StatResponse response = mapFromJson(content, StatResponse.class);
        assertEquals(java.util.Optional.of(Long.valueOf(THREADS_NUMBER)), java.util.Optional.ofNullable(response.getAllCounter()));
    }
}

