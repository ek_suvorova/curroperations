import app.data.entity.OperationsCounter;
import app.data.entity.OperationsPerCurrencyCounter;
import app.actionstat.model.StatResponse;
import app.data.repository.OperationsCounterRepository;
import app.data.repository.OperationsPerCurrencyCounterRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Проверка /stat запроса с предварительной загрузкой данных в БД
 */
@Transactional
public class StatRequestTest extends AbstractBaseTest {
    private static final long ONE_COUNTER = 1L;
    private static final long FIVE_COUNTER = 5L;
    private static final int ZERO_SIZE = 0;
    private static final int ONE_SIZE = 1;

    @Resource
    private OperationsCounterRepository operationsCounterRepository;

    @Resource
    private OperationsPerCurrencyCounterRepository operationsPerCurrencyCounterRepository;

    @Test
    public void sendStatRequestWithEmptyDB() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(STAT_SERVICE_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);
        String content = mvcResult.getResponse().getContentAsString();
        StatResponse response = mapFromJson(content, StatResponse.class);
        assertNull(response.getAllCounter());
        assertNull(response.getSuccessCounter());
        assertEquals(ZERO_SIZE, response.getSuccessPerCurrencyCounter().size());
    }

    @Test
    public void sendStatRequestWithDefinedDB() throws Exception {
        OperationsCounter operationsCounter = new OperationsCounter();
        operationsCounter.setAllOperations(FIVE_COUNTER);
        operationsCounter.setSuccessOperations(ONE_COUNTER);
        operationsCounterRepository.save(operationsCounter);

        OperationsPerCurrencyCounter operationsPerCurrencyCounter = new OperationsPerCurrencyCounter();
        operationsPerCurrencyCounter.setCurrencyCode(RUR_CODE);
        operationsPerCurrencyCounter.setSuccessCounter(ONE_COUNTER);
        operationsPerCurrencyCounterRepository.save(operationsPerCurrencyCounter);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(STAT_SERVICE_URL)
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(HttpStatus.OK.value(), status);
        String content = mvcResult.getResponse().getContentAsString();
        StatResponse response = mapFromJson(content, StatResponse.class);
        assertEquals(java.util.Optional.of(FIVE_COUNTER), java.util.Optional.ofNullable(response.getAllCounter()));
        assertEquals(java.util.Optional.of(ONE_COUNTER), java.util.Optional.ofNullable(response.getSuccessCounter()));
        Map<String, Long> perCurrencyCounter = response.getSuccessPerCurrencyCounter();
        assertEquals(ONE_SIZE, perCurrencyCounter.size());
        assertEquals(java.util.Optional.of(ONE_COUNTER), java.util.Optional.ofNullable(perCurrencyCounter.get(RUR_CODE)));
    }

}
